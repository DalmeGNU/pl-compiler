// Todavia no tenemos variables globales !!
// struct {
// int x;
// int y;
// } par;

int main()
{
	typedef struct {
		int x;
		int y;
	} par;

	par *pares;
	pares = new(par, 2);
	pares[0].x = 1;
	pares[0].y = 1;
	(*pares).x = 1;
	(*pares).y = 1;
	pares[1].x = 1;
	pares[1].y = 1;
	return 0;
}
