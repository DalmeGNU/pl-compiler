/* C/C++ program to solve N Queen Problem using
backtracking */

// Code taken from https://www.geeksforgeeks.org/c-program-for-n-queen-problem-backtracking-3/
#define N 8
#include <stdbool.h>
#include <stdio.h>

/* A utility function to print solution */
void printSolution(int board[8][8])
{
	int i; i = 0;
	int j;
	while (i < N) {
		j = 0;
		while (j < N) {
			if (board[i][j] == 1)
				printf("%d\n", j);

			j = j + 1;
		}
		i = i + 1;
	}
}

/* A utility function to check if a queen can
be placed on board[row][col]. Note that this
function is called when "col" queens are
already placed in columns from 0 to col -1.
So we need to check only left side for
attacking queens */
bool isSafe(int board[8][8], int row, int col)
{
	int i, j;

	/* Check this row on left side */
	i = 0;
	while (i < col) {
		if (board[row][i] == 1)
			return false;

		i = i + 1;
	}

	/* Check upper diagonal on left side */
	i = row;
	j = col;
	while (i >= 0 && j >= 0) {
		if (board[i][j] == 1)
			return false;
		i = i - 1;
		j = j - 1;
	}

	/* Check lower diagonal on left side */
	i = row;
	j = col;
	while (j >= 0 && i < N) {
		if (board[i][j] == 1)
			return false;
		i = i + 1;
		j = j - 1;
	}

	return true;
}

/* A recursive utility function to solve N
Queen problem */
bool solveNQUtil(int board[8][8], int col)
{
	/* base case: If all queens are placed
	then return true */
	if (col >= N)
		return true;

	/* Consider this column and try placing
	this queen in all rows one by one */
	int i; i = 0;
	while (i < N) {
		/* Check if the queen can be placed on
		board[i][col] */
		if (isSafe(board, i, col)) {
			/* Place this queen in board[i][col] */
			board[i][col] = 1;

			/* recur to place rest of the queens */
			if (solveNQUtil(board, col + 1))
				return true;

			/* If placing queen in board[i][col]
			doesn't lead to a solution, then
			remove queen from board[i][col] */
			board[i][col] = 0; // BACKTRACK
		}
		i = i + 1;
	}

	/* If the queen cannot be placed in any row in
		this column col then return false */
	return false;
}

/* This function solves the N Queen problem using
Backtracking. It mainly uses solveNQUtil() to
solve the problem. It returns false if queens
cannot be placed, otherwise, return true and
prints placement of queens in the form of 1s.
Please note that there may be more than one
solutions, this function prints one of the
feasible solutions.*/
bool solveNQ()
{
	int board[8][8];
	int i;
	int j;
	i = 0;
	while (i < N) {
		j = 0;
		while (j < N) {
			board[i][j] = 0;
			j = j + 1;
		}
		i = i + 1;
	}

	if (solveNQUtil(board, 0) == false) {
		//printf("Solution does not exist");
		return false;
	}

	printSolution(board);
	return true;
}

// driver program to test above function
int main()
{
	solveNQ();
	return 0;
}

