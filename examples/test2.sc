struct { int a; int b; } f() {
	struct { int a; int b; } res;
	res.a = 0;
	res.b = 1;
	return res;
}

struct { int a; int b; } g(struct {int a; int b; } r) {
	r.a = r.a + 1;
	r.b = r.b + 1;
	return r;
}

int main()
{
	struct { int a; int b; } r;
	r = g(g(g(f())));
	print(r.a);
	print(r.b);
}
