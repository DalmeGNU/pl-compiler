int factorial(int n)
{
	if (n == 0)
		return 1;
	else
		return n · factorial(n-1);

	// hay que darle SIEMPRE un return al final
	// ya que wasm no es capaz de ver que la situacion
	// anterior cubre todos los casos
	// El compilador mete por defecto un return 0 si la
	// ultima instruccion no es un return
	//return 0;
}

int main()
{
	print(factorial(7));
	return 0;
}
