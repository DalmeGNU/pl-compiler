// Code taken from https://www.geeksforgeeks.org/c-program-for-n-queen-problem-backtracking-3/

int printSolution(int board[8][8])
{
	int N; N = 8;
	int i; i = 0;
	int j;
	while (i < N) {
		j = 0;
		while (j < N) {
			if (board[i][j] == 1)
				print(j);

			j = j + 1;
		}
		i = i + 1;
	}
	return 0;
}

bool isSafe(int board[8][8], int row, int col)
{
	int N; N = 8;
	int i; int j;

	// Check this row on left side */
	i = 0;
	while (i < col) {
		if (board[row][i] == 1)
			return false;

		i = i + 1;
	}

	// Check upper diagonal on left side */
	i = row;
	j = col;
	while (i >= 0 && j >= 0) {
		if (board[i][j] == 1)
			return false;
		i = i - 1;
		j = j - 1;
	}

	// Check lower diagonal on left side */
	i = row;
	j = col;
	while (j >= 0 && i < N) {
		if (board[i][j] == 1)
			return false;
		i = i + 1;
		j = j - 1;
	}

	return true;
}

bool solveNQUtil(int board[8][8], int col)
{
	int N; N = 8;
	if (col >= N) {
		printSolution(board);
		return true;
	}

	int i; i = 0;
	while (i < N) {
		// Check if the queen can be placed on
		//board[i][col] */
		if (isSafe(board, i, col)) {
			// Place this queen in board[i][col] */
			board[i][col] = 1;

			// recur to place rest of the queens */
			if (solveNQUtil(board, col + 1))
				return true;

			// If placing queen in board[i][col]
			//doesn't lead to a solution, then
			//remove queen from board[i][col] */
			board[i][col] = 0; // BACKTRACK
		}
		i = i + 1;
	}

	// If the queen cannot be placed in any row in
	//	this column col then return false */
	return false;
}

bool solveNQ()
{
	int N; N = 8;
	int board[8][8];
	int i;
	int j;
	i = 0;
	while (i < N) {
		j = 0;
		while (j < N) {
			board[i][j] = 0;
			j = j + 1;
		}
		i = i + 1;
	}

	if (solveNQUtil(board, 0) == false) {
		//printf("Solution does not exist");
		print(-1);
		return false;
	}

	return true;
}

// driver program to test above function
int main()
{
	solveNQ();
	return 0;
}

