// We pass it by copy
int printMatrix(int matrix[2][3]) {
	int i; i = 0;
	while (i < 2) {
		int j; j = 0;
		while (j < 3) {
			print(matrix[i][j]);
			j = j + 1;
		}
		i = i + 1;
	}
}

int printMatrixPtr(int **matrix) {
	int i; i = 0;
	while (i < 2) {
		int j; j = 0;
		while (j < 3) {
			print(matrix[i][j]);
			j = j + 1;
		}
		i = i + 1;
	}
}

int main()
{
	int matrix[2][2][3];
	int i; i = 0;
	while (i < 2) {
		int j; j = 0;
		while (j < 3) {
			matrix[1][i][j] = 3·i + j;
			j = j + 1;
		}
		i = i + 1;
	}
	printMatrix(matrix[1]);
	int **pmatrix;
	pmatrix = new(int*, 2);
	pmatrix[0] = matrix[1][0];
	pmatrix[1] = matrix[1][1];
	printMatrixPtr(pmatrix);
	matrix[0] = matrix[1];
	printMatrix(matrix[0]);
}
