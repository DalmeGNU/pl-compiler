// This tests if we free the VLA arrays memory correctly or not
// We use a pointer to the VLA array original position so we can
// se through the execution that other expected values are being placed
// in the vary same positions. So, if everything works nicely we should
// get the array [1..n] on the screen, and then 2*[1..n] and 0 (old MP), 16 (old SP value)
// and then 3 and 6
int f() {
	int array[4];
	array[0] = 3;
	array[1] = 6;
}

int main()
{
	int n; n = 4;
	int *ptr; int i;
	if (n > 0) {
		int array[n];
		i = 0;
		ptr = array;
		while (i < n) {
			array[i] = i+1;
			i = i + 1;
		}
	}
	i = 0;
	while (i < n) {
		print(ptr[i]);
		i = i + 1;
	}
	if (n > 0) {
		{
			int array2[n];
			i = 0;
			while (i < n) {
				array2[i] = 2·(i+1);
				i = i + 1;
			}
		}
	}
	i = 0;
	while (i < n) {
		print(ptr[i]);
		i = i + 1;
	}
	f();
	i = 0;
	while (i < n) {
		print(ptr[i]);
		i = i + 1;
	}

}
