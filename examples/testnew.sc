int* f(int *p) {
	return new(int, 7);
}

int main()
{
	int* ptr;
	ptr = new(int, 15);
	f(new (int, 9));
	*ptr = 55;
	print(*ptr);
	ptr[2] = 56;
	ptr[1] = 0;
	print(ptr[2]);
	print(ptr[0]);
	// this should fail in execution time
	ptr = new(int, -7);
}
