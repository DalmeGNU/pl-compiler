cd main/lexer
java -cp jflex.jar jflex.Main lexer.l
cd ..
cd parser
java -cp ../../cup.jar java_cup.Main -parser Parser -symbols TokenType -nopositions Parser.cup
cd ..
javac -cp "../cup.jar" *.java ast/*.java parser/*.java lexer/*.java
cd ..
