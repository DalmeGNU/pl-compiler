package main.lexer;

import java.util.function.*;
import main.parser.TokenType;

%%
%cup
%char
%line
%column
%class Lexer
%type  Token
%unicode
%ctorarg Consumer<Token> action
%public

%init{
  this.action = action;
%init}

%{
  public boolean errors = false;
  private Consumer<Token> action;

  private Token makeToken(int type, String str) {
	Token r = new Token(yyline+1, yycolumn+1, type, str);
	action.accept(r);
	return r;
  }

  private Token makeToken(int type) {
	return makeToken(type, null);
  }
%}

%eofval{
  return makeToken(TokenType.EOF);
%eofval}

letra  = ([A-Z]|[a-z])
digitoPositivo = [1-9]
digito = ({digitoPositivo}|0)
intNumber = {digito}{digito}*
separador = [ \t\r\b\n]
comentario = \/\/[^\n]* 
true = true
false = false
int = int
bool = bool
struct = struct
if = if
else = else
while = while
return = return
print = print
read = read
typedef = typedef
identifier = {letra}({letra}|{digito}|\_)*
sumOp = \+
minusOp = \-
multOp = ·
divOp = \/
andOp = &&
orOp = \|\|
structMember = \.
endStatement = ;
beginArrayIndex = \[
endArrayIndex = \]
beginBlock = \{
endBlock = \}
parenthesisOpening = \(
parenthesisClosing = \)
assignOp = \=
equalOp = \=\=
notEqualOp = \!\=
minusEqualOp = <\=
greaterEqualOp = >\=
minorOp = <
greaterOp = >
pointerOp = \*
comma = ,
new = new
%%
{separador}               {}
{comentario}              {}
{int}			  {return makeToken(TokenType.INT);}
{bool}			  {return makeToken(TokenType.BOOL);}
{true}			  {return makeToken(TokenType.TRUE);}
{false}			  {return makeToken(TokenType.FALSE);}
{struct}		  {return makeToken(TokenType.STRUCT);}
{if}			  {return makeToken(TokenType.IF);}
{else}			  {return makeToken(TokenType.ELSE);}
{while}			  {return makeToken(TokenType.WHILE);}
{return}		  {return makeToken(TokenType.RETURN);}
{print}			  {return makeToken(TokenType.PRINT);}
{read}			  {return makeToken(TokenType.READ);}
{typedef}		  {return makeToken(TokenType.TYPEDEF);}
{new}			  {return makeToken(TokenType.NEW);}
{beginArrayIndex}         {return makeToken(TokenType.BEGIN_ARRAY_INDEX);}
{endArrayIndex}		  {return makeToken(TokenType.END_ARRAY_INDEX);}
{beginBlock}		  {return makeToken(TokenType.BEGIN_BLOCK);}
{endBlock}		  {return makeToken(TokenType.END_BLOCK);}
{endStatement}		  {return makeToken(TokenType.SEMICOLON);}
{structMember}		  {return makeToken(TokenType.STRUCT_MEMBER);}
{sumOp}			  {return makeToken(TokenType.SUM_OP);}
{minusOp}		  {return makeToken(TokenType.MINUS_OP);}
{multOp}		  {return makeToken(TokenType.MULT_OP);}
{divOp}			  {return makeToken(TokenType.DIV_OP);}
{parenthesisOpening}      {return makeToken(TokenType.POP);}
{parenthesisClosing}      {return makeToken(TokenType.PCL);} 
{assignOp}		  {return makeToken(TokenType.ASSIGN_OP);}
{equalOp}		  {return makeToken(TokenType.EQUAL_OP);}
{notEqualOp}		  {return makeToken(TokenType.NOT_EQUAL_OP);}
{minusEqualOp}		  {return makeToken(TokenType.LOWER_EQUAL_OP);}
{greaterEqualOp}	  {return makeToken(TokenType.GREATER_EQUAL_OP);}
{minorOp}		  {return makeToken(TokenType.LOWER_OP);}
{greaterOp}		  {return makeToken(TokenType.GREATER_OP);}
{intNumber}		  {return makeToken(TokenType.INT_VALUE, yytext());}
{identifier}		  {return makeToken(TokenType.IDENTIFIER, yytext());}
{andOp}			  {return makeToken(TokenType.AND_OP);}
{orOp}			  {return makeToken(TokenType.OR_OP);}
{pointerOp}		  {return makeToken(TokenType.POINTER_OP);}
{comma}			  {return makeToken(TokenType.COMMA);}
[^]                       {System.out.println("Error:" + (yyline+1) + ":" + (yycolumn+1) + ": Caracter inesperado: " + yytext()); errors = true; }  
