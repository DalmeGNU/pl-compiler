package main.lexer;

import java_cup.runtime.Symbol;

public class Token extends Symbol {
    private String _lexema;
    private int _row;
    private int _column;
    private int _type;
    public Token(int row, int column, int type) {
	super(type, new TokenValue(row,column));
    }
    public Token(int row, int column, int type, String lexema) {
	super(type, new TokenValue(lexema,row,column));
    }
    public int type () {return sym;}
    public String lexema() {return ((TokenValue)value).lexema;}
    public int row() {return ((TokenValue)value).fila;}
    public int column() {return ((TokenValue)value).columna;}
    public String toString() {
/*	if (lexema() == null) {
	    return "[clase:"+TokenType.str_type[type()]+",fila:"+row()+",col:"+column()+"]";
	} else {
	    return "[clase:"+TokenType.str_type[type()]+",fila:"+row()+",col:"+column()+",lexema:"+lexema()+"]";
	}*/
	return null;
    }
}
