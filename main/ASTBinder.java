package main;

import main.ast.*;

import java.util.*;

public final class ASTBinder {
	public static boolean errors = false;

	private ArrayList<Hashtable<String, ASTNode>> stack;

	private void addSymbol(Hashtable<String, ASTNode> table, String s_name, ASTNode s_declaration) throws Exception {
		if (table.containsKey(s_name)) {
			System.out.println("Error:" + s_declaration.error_pos() + " Symbol " + s_name + " is defined twice in this scope");
			ASTBinder.errors = true;
		}
		else
			table.put(s_name, s_declaration);
	}

	private ASTNode checkSymbol(String symbol, String error_pos) throws Exception {
		for (int i = stack.size() - 1; i >= 0; i--) {
			if (stack.get(i).containsKey(symbol))
				return stack.get(i).get(symbol);
		}
		System.out.println("Error:" + error_pos + " Symbol " + symbol + " used, but never defined");
		ASTBinder.errors = true;
		return null;
	}

	private void bind_type(Type t) throws Exception {
		switch (t.kind()) {
			case POINTER:
				bind_type(((Pointer_Type)t).getPointedType());
				break;
			case ARRAY:
				bind_type(((Array_Type)t).getType());
				bind_exp(((Array_Type)t).getExp());
				break;
			case STRUCT:
				Struct_Type st_type = (Struct_Type) t;
				ArrayList<Declaration> l = st_type.getDeclarations();
				for (Declaration d : l)
					bind_type(d.getType());
				break;
			case CUSTOM:
				String type_name = ((Custom_Type)t).getName();
				ASTNode b_node = checkSymbol(type_name, t.error_pos());
				((Custom_Type)t).setbind(b_node);
				break;
		}
	}

	private void bind_exp(E exp) throws Exception {
		switch (exp.kind()) {
			case DESIGNATOR:
				bind_designator(exp);
				break;
			case OR:
			case AND:
			case EQUAL:
			case NOT_EQUAL:
			case ARITHMETIC:
				bind_exp(exp.opnd1());
				bind_exp(exp.opnd2());
				break;
			case FUNCTIONCALL:
				FunctionCallExpression f = (FunctionCallExpression) exp;
				ASTNode bind_node = checkSymbol(f.getCall().getName(), f.error_pos());
				f.setbind(bind_node);
				for (E e : f.getCall().getArgs())
					bind_exp(e);

				break;
			case NEW:
				NewExpression n = (NewExpression) exp;
				bind_type(n.getType());
				bind_exp(n.getExp());
				break;
		}
	}

	private void bind_designator(E designator) throws Exception {
		if (designator.kind() != EKind.DESIGNATOR) {
			System.out.println("Error:" + designator.error_pos() + " " + designator + " isn't a valid designator");
			ASTBinder.errors = true;
		}
		else {
			Designator d = (Designator) designator;
			switch (d.dkind()) {
				case POINTER:
					PointerDesignator p = (PointerDesignator) d;
					bind_designator(p.getPointedDesignator());
					break;
				case ARRAY:
					ArrayDesignator a = (ArrayDesignator) d;
					bind_designator(a.getDesignator());
					bind_exp(a.getExp());
					break;
				case STRUCT:
					StructDesignator s = (StructDesignator) d;
					bind_designator(s.getDesignator());
					break;
				case ATOMIC:
					ASTNode b_node = checkSymbol(((AtomicDesignator)d).getName(), d.error_pos());
					d.setbind(b_node);
					break;
			}
		}
	}

	private void bind_statement(Statement s) throws Exception {
		Hashtable<String, ASTNode> cur_scope = stack.get(stack.size() - 1);
		switch (s.kind()) {
			case DECLARATION:
				DeclarationStatement ds = (DeclarationStatement) s;
				Declaration d = ds.getDeclaration();
				bind_type(d.getType());
				addSymbol(cur_scope, d.getName(), ds);
				break;
			case WHILE:
				WhileStatement w = (WhileStatement) s;
				bind_exp(w.getCond());
				bind_statement(w.getBody());
				break;
			case PRINT:
				PrintStatement p = (PrintStatement) s;
				bind_exp(p.getExp());
				break;
			case RETURN:
				ReturnStatement r = (ReturnStatement) s;
				bind_exp(r.getExp());
				break;
			case BLOCK:
				BlockStatement b = (BlockStatement) s;
				stack.add(new Hashtable<String, ASTNode>());
				for (Statement st : b.getBlock())
					bind_statement(st);

				stack.remove(stack.size() - 1);
				break;
			case FUNCTIONCALL:
				FunctionCallStatement f = (FunctionCallStatement) s;
				ASTNode bind_node = checkSymbol(f.getCall().getName(), f.error_pos());
				f.setbind(bind_node);
				for (E e : f.getCall().getArgs())
					bind_exp(e);

				break;
			case TYPEDECLARATION:
				TypeDeclarationStatement t = (TypeDeclarationStatement) s;
				Declaration d_ = t.getDeclaration();
				bind_type(d_.getType());
				addSymbol(cur_scope, d_.getName(), t);
				break;
			case READ:
				ReadStatement rd = (ReadStatement) s;
				E des = rd.getDesignator();
				// syntactically we allow designator to be
				// expressions (to allow the use of ())
				// now we have to check if the designator is
				// well formed or not (bind_designator binds and
				// checks for this)
				bind_designator(des);
				break;
			case IFELSE:
				IfElseStatement i = (IfElseStatement) s;
				bind_exp(i.getCond());
				bind_statement(i.getIf());
				bind_statement(i.getElse());
				break;
			case IF:
				IfStatement ifstm = (IfStatement) s;
				bind_exp(ifstm.getCond());
				bind_statement(ifstm.getIf());
				break;
			case ASSIGNMENT:
				AssignmentStatement a = (AssignmentStatement) s;
				bind_designator(a.getDesignator());
				bind_exp(a.getExp());
				break;
		}
	}

	private void bind_function(FunctionDefinition f) throws Exception {
		Hashtable<String, ASTNode> f_scope = new Hashtable<>();
		stack.add(f_scope);
		ArrayList<Statement> slst = f.getBody();
		ArrayList<Declaration> args = f.getArgs();
		bind_type(f.getType());
		for (Declaration d : args) {
			bind_type(d.getType());
			addSymbol(f_scope, d.getName(), f);
		}
		for (Statement s : slst) {
			bind_statement(s);
		}
		stack.remove(stack.size() - 1);
	}

	public void bind(Program p) throws Exception {
		stack = new ArrayList<>();
		Hashtable<String, ASTNode> bind_scope = new Hashtable<>();
		stack.add(bind_scope);
		ArrayList<FunctionDefinition> functions = p.getFunctions();
		for (FunctionDefinition f: functions) {
			String name = f.getName();
			if (bind_scope.containsKey(name)) {
				System.out.println("Error:" + f.error_pos()  + " Function " + name + " is defined twice");
				ASTBinder.errors = true;
			}
			else {
				bind_scope.put(name, f);
			}
			bind_function(f);
		}
	}
}
