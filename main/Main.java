package main;

import java.util.function.*;
import java.io.Reader;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.FileWriter;
import java.io.BufferedWriter;

import main.lexer.*;
import main.parser.*;
import main.ast.*;

import java_cup.runtime.Symbol;

public class Main {
	public static void main(String[] args) throws FileNotFoundException, IOException, Exception {
		String path = null;
		boolean debug_mode = false;
		for (String a : args) {
			if (a.length() >= 2 && a.charAt(0) == '-' && a.charAt(1) == 'd') {
				debug_mode = true;
			} else {
				path = a;
			}
		}
		if (path == null) {
			System.out.println("Error: No input file provided");
			return;
		}

		Consumer<Token> action;
		if (debug_mode)
			action = (Token a) -> System.out.println(a);
		else
			// do nothing
			action = a -> {};

		Reader input = new InputStreamReader(new FileInputStream(path));
		Lexer lex = new Lexer(input, action);
		Parser parser = new Parser(lex);
		Program result = (Program)parser.parse().value;
		if (lex.errors) {
			System.out.println("Too many lexical errors. Aborting.. !");
			System.exit(-1);
		}
		if (parser.syntax_errors) {
			System.out.println("Too many syntax errors. Aborting.. !");
			System.exit(-1);
		}
		System.out.println(result);
		ASTBinder binder = new ASTBinder();
		binder.bind(result);
		if (ASTBinder.errors) {
			System.out.println("Too many binding errors. Aborting.. !");
			System.exit(-1);
		}
		ASTTyper typer = new ASTTyper();
		typer.typeAST(result);
		if (ASTTyper.errors) {
			System.out.println("Too many type errors. Aborting.. !");
			System.exit(-1);
		}
		String file = path + ".wat";
		try (FileWriter fw = new FileWriter(file);
		     BufferedWriter bw = new BufferedWriter(fw)) {
			bw.write(result.generateCode());
		}
	}
}
