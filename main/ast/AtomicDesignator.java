package main.ast;

public class AtomicDesignator extends Designator {
	private String identifier;
	private int row;
	private int column;
	public AtomicDesignator(String i, int arow, int acolumn) {
		identifier = i;
		row = arow;
		column = acolumn;
	}
	public String getName() { return identifier; }
	public DesignatorKind dkind() { return DesignatorKind.ATOMIC; }
	public String toString() {return identifier;}
	public String generateCode() {
		// we removed type declarations during the typing process
		// so the binding should be a DeclarationStatement
		if (this.bind().nodeKind() == NodeKind.STATEMENT) {
			Declaration d = ((DeclarationStatement)this.bind()).getDeclaration();
			return "i32.const " + Delta.getPos(d) +
				"\nget_global $current_frame\ni32.add\n";
		}
		else if (this.bind().nodeKind() == NodeKind.FUNCTIONDEFINITION) {
			Declaration d = null;
			for (Declaration ds : ((FunctionDefinition)this.bind()).getArgs()) {
				if (ds.getName().equals(identifier)) {
					d = ds;
					break;
				}
			}
			if (d == null) {
				System.out.println("Compiler fatal error: " + identifier + " not found in " + this.bind());
				System.exit(-1);
			}
			return "i32.const " + Delta.getPos(d) +
				"\nget_global $current_frame\ni32.add\n";
		}
		System.out.println("Compiler fatal error: Unexpected binding " + this.bind() + " for " + identifier);
		System.exit(-1);
		return null;
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
