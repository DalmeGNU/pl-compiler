package main.ast;

public class StructDesignator extends Designator {
	private E designator;
	private String structfield;
	private int row;
	private int column;
	public StructDesignator(E d, String f, int arow, int acolumn) {
		designator = d;
		structfield = f;
		row = arow;
		column = acolumn;
	}
	public E getDesignator() { return designator; }
	public String getField() { return structfield; }
	public DesignatorKind dkind() { return DesignatorKind.STRUCT; }
	public String toString() {return "(" + designator + ")." + structfield;}
	public String generateCode() {
		return designator.generateCode() + "i32.const " +
			((Struct_Type)designator.type()).delta(structfield) + "\ni32.add\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
