package main.ast;

public class PrintStatement extends Statement {
	public StatementKind kind() {return StatementKind.PRINT;}
	private E exp;
	private int row;
	private int column;
	public PrintStatement(E e, int arow, int acolumn) {
		exp = e;
		row = arow;
		column = acolumn;
	}
	public E getExp() { return exp; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "Print(" + exp + ")";}
	public String generateCode() {
		String res = exp.generateCode();
		if (exp.kind() == EKind.DESIGNATOR) {
			res = res + "i32.load\n";
		}
		return res + "call $print\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
