package main.ast;

public abstract class E implements ASTNode {
	private Type _type__;
	public abstract EKind kind();
	public E opnd1() {throw new UnsupportedOperationException("opnd1");}
	public E opnd2() {throw new UnsupportedOperationException("opnd2");} 
	public String num() {throw new UnsupportedOperationException("num");}
	public NodeKind nodeKind() {return NodeKind.EXPRESSION;}
	public String toString() {return "";}
	public Type type() { return _type__; }
	public void settype(Type t) { _type__ = t; }
	public int getRow() { return 0; }
	public int getColumn() { return 0; }
}
