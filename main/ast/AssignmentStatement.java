package main.ast;

public class AssignmentStatement extends Statement {
	public StatementKind kind() {return StatementKind.ASSIGNMENT;}
	private E designator, exp;
	private int row;
	private int column;
	public AssignmentStatement(E designator, E e, int arow, int acolumn) {
		this.designator = designator;
		exp = e;
		row = arow;
		column = acolumn;
	}
	public E getDesignator() { return designator; }
	public E getExp() { return exp; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "Assign(" + designator  + "," + exp + ")";}
	public String generateCode() {
		// this is the "special case" of compatibility between
		// pointers and arrays
		if (exp.type().kind() == TypeKind.ARRAY && designator.type().kind() == TypeKind.POINTER) {
			String res = designator.generateCode() + exp.generateCode();
			if (((Array_Type)exp.type()).getExp().kind() != EKind.INTVALUE)
				res = res + "i32.load\n";
			return res + "i32.store\n";
		}

		// from now on, the type of the designator and the exp
		// are the same

		if (exp.type().kind() == TypeKind.STRUCT || exp.type().kind() == TypeKind.ARRAY) {
			if (exp.kind() == EKind.DESIGNATOR) {
				int t_size = exp.type().size();
				return exp.generateCode() + "i32.const " + t_size + "\n" +
					designator.generateCode() + "call $mcopy\n";
			} else if (exp.kind() == EKind.FUNCTIONCALL) {
				int t_size = exp.type().size();
				return exp.generateCode() + "get_global $SP\ni32.const 8\n" +
					"i32.add\ni32.const " + t_size + "\n" +
					designator.generateCode() + "call $mcopy\n";
			} else {
				System.out.println("Compiler fatal error: Expression " + exp + " is of type struct but isn't a designator or a functioncall");
				System.exit(-1);
				return null;
			}
		}
		else {
			if (exp.kind() == EKind.DESIGNATOR)
				return designator.generateCode() + exp.generateCode() + "i32.load\ni32.store\n";
			else
				return designator.generateCode() + exp.generateCode() + "i32.store\n";
		}

	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
