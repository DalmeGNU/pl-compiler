package main.ast;

import java.util.ArrayList;

public class FunctionDefinition implements ASTNode {
	private String name;
	private Type type;
	private ArrayList<Declaration> args;
	private ArrayList<Statement> block;
	private int row;
	private int column;
	public FunctionDefinition(String fname, Type ftype, ArrayList<Declaration> fargs, int arow, int acolumn) {
		name = fname;
		type = ftype;
		args = fargs;
		row = arow;
		column = acolumn;
	}
	public String getName() { return name; }
	public ArrayList<Declaration> getArgs() { return args; }
	public ArrayList<Statement> getBody() { return block; }
	public void addBlock(ArrayList<Statement> b) { block = b; }
	public NodeKind nodeKind() {return NodeKind.FUNCTIONDEFINITION;}
	public String toString() {return name + "(" + args + ") {" + block + "}";}
	public Type getType() { return type; }
	public String generateCode() {
		Delta.reset();
		for (Declaration d : args)
			Delta.add(d, d.getType().size());

		if (name.equals("mcopy") || name.equals("checkNPSP") || name.equals("reserveHeap")) {
			System.out.println("Error: " + name + " is a reserved function name of the language");
			System.exit(-1);
		}

		String res = "(func $" + name + "\n";
		// main is always void
		if (type.kind() != TypeKind.STRUCT && !name.equals("main")) {
			res = res + "(result i32)\n";
		}
		// tmp is a variable we can use whenever we want
		res = res + "(local $curSP i32)\n(local $tmp i32)\n(local $tmp2 i32)\n";

		MutableInt c = new MutableInt();
		MutableInt max = new MutableInt();
		MutableInt VLAarray = new MutableInt();
		MutableInt anyVLAarray = new MutableInt();
		for (Statement s : block)
			s.maxBlocks(c, max, VLAarray, anyVLAarray);

		// if we have at least one block and there is any VLA array on the function
		// then we'll have VLA support working
		// note that in case that there are only function-global VLA arrays and there
		// are blocks this condition will be true, but we will only declare this variables
		// as no block will ever use them
		if (max.x > 0 && anyVLAarray.x == 1) {
			for (int i = 1; i <= max.x; i++)
				res = res + "(local $block" + i + " i32)\n";
		}

		if (name.equals("main")) {
			res = res + "i32.const " + maxMemory() + "\nset_global $SP\n";
		}
		for (Statement s : block)
			res = res + s.generateCode();

		if (type.kind() != TypeKind.STRUCT && !name.equals("main")) {
			if (block.isEmpty() || block.get(block.size()-1).kind() != StatementKind.RETURN)
				res = res + ";; default final return\ni32.const 0\nreturn\n";
		}
		return res + ")\n";
	}

	public int maxMemory() {
		MutableInt c = new MutableInt();
		MutableInt max = new MutableInt();
		for (Declaration d : args) {
			c.x += d.getType().size();
			max.x += d.getType().size();
		}
		for (Statement s : block)
			s.maxMemory(c, max);

		return max.x;
	}

	public String error_pos() {
		return "" + row + ":" + column;
	}
}
