package main.ast;

public class Array_Type extends Pointer_Compatible_Type {
	private E exp;
	private Type type;
	public TypeKind kind() {return TypeKind.ARRAY;}
	public Array_Type(Type t, E e, int row, int column) {
		type = t;
		exp = e;
		this.row = row;
		this.column = column;
	}
	public Type getType() { return type; }
	public E getExp() { return exp; }
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return "Array(" + type + "," + exp + ")";}
	public int size() {
		// solo arrays estaticos (de momento)
		if (exp.kind() == EKind.INTVALUE) {
			return type.size() * ((IntegerExpression)exp).getValue();
		}
		// else (array dinamico)
		return 4;
	}
	public boolean hasVariableLength() {
		return (exp.kind() != EKind.INTVALUE) || type.hasVariableLength();
	}
}
