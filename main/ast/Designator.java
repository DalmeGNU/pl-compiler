package main.ast;

public abstract class Designator extends E {
	private ASTNode bind_ = null;
	public ASTNode bind() { return bind_; }
	public void setbind(ASTNode b) { bind_ = b; }
	public NodeKind nodeKind() {return NodeKind.EXPRESSION;}
	public EKind kind() {return EKind.DESIGNATOR; }
	public abstract DesignatorKind dkind();
	public String toString() {return "";}
}
