package main.ast;

public class Bool_Type extends Type {
	public Bool_Type(int row, int column) {
		this.row = row;
		this.column = column;
	}
	public TypeKind kind() {return TypeKind.BOOL;}
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return "BOOL";}
	public int size() {
		return 4;
	}
}
