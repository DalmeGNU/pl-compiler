package main.ast;

public class ANDExpression extends E {
	public EKind kind() {return EKind.AND;}
	private E e1;
	private E e2;
	private int row;
	private int column;
	public ANDExpression(E a, E b, int arow, int acolumn) {
		e1 = a;
		e2 = b;
		row = arow;
		column = acolumn;
	}
	public String toString() {return "&&(" + e1 + "," + e2 + ")";}
	public E opnd1() { return e1; }
	public E opnd2() { return e2; }
	public String generateCode() {
		String ret = ";; " + this.toString() + "\n" + e1.generateCode();
		if (e1.kind() == EKind.DESIGNATOR)
			ret = ret + "i32.load\n";
		ret = ret + e2.generateCode();
		if (e2.kind() == EKind.DESIGNATOR)
			ret = ret + "i32.load\n";

		return ret + "i32.and\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
