package main.ast;

public class FunctionCallExpression extends E {
	public EKind kind() {return EKind.FUNCTIONCALL;}
	private ASTNode bind_;
	private FunctionCall fcall;
	public FunctionCallExpression(FunctionCall f) {
		fcall = f;
		bind_ = null;
	}
	public ASTNode bind() { return bind_; }
	public FunctionCall getCall() { return fcall; }
	public void setbind(ASTNode b) { bind_ = b; fcall.setbind(b); }
	public String toString() {return "" + fcall;}
	public String generateCode() { return fcall.generateCode(); }
	public String error_pos() { return fcall.error_pos(); }
}
