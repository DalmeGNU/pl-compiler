package main.ast;

public class NewExpression extends E {
	public EKind kind() {return EKind.NEW;}
	private Type type;
	private E size;
	private int row;
	private int column;
	public NewExpression(Type t, E s, int arow, int acolumn) {
		type = t;
		size = s;
		row = arow;
		column = acolumn;
	}
	public Type getType() { return type; }
	public E getExp() { return size; }
	public String toString() {return "New(" + type + "," + size + ")";}
	// We suppose size is positive
	// size <= 0 is undefined behavior
	public String generateCode() {
		String res = size.generateCode();
		if (size.kind() == EKind.DESIGNATOR)
			res = res + "i32.load\n";

		res = res + "i32.const " + type.size() + "\ni32.mul\ncall $reserveHeap\n" +
			"get_global $NP\n";
		return res;
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
