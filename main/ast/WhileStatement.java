package main.ast;

public class WhileStatement extends Statement {
	public StatementKind kind() {return StatementKind.WHILE;}
	private E condition;
	private Statement body;
	private int row;
	private int column;
	public WhileStatement(E c, Statement s, int row, int column) {
		condition = c;
		body = s;
		this.row = row;
		this.column = column;
	}
	public E getCond() { return condition; }
	public Statement getBody() { return body; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "While(" + condition + "," + body + ")";}
	public String generateCode() {
		return "block\nloop\n" + condition.generateCode() + "i32.eqz\nbr_if 1\n" +
			body.generateCode() + "br 0\nend\nend\n";
	}
	public void maxMemory(MutableInt c, MutableInt max) {
		body.maxMemory(c,max);
	}
	public void maxBlocks(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {
		body.maxBlocks(c,max,VLAarray,anyVLAarray);
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
