package main.ast;

public class Custom_Type extends Type {
	// CUSTOM means it's an identifier we have to check
	private String name;
	private ASTNode bind_;
	public Custom_Type(String type_name, int row, int column) {
		name = type_name;
		this.row = row;
		this.column = column;
	}
	public ASTNode bind() { return bind_; }
	public void setbind(ASTNode b) { bind_ = b; }
	public String getName() { return name; }
	public TypeKind kind() {return TypeKind.CUSTOM;}
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return name;}
	public int size() {
		System.out.println("Compiler fatal error: Type " + name + " wasn't succesfully extended during type checking phase");
		System.exit(-1);
		return -1;
	}
}
