package main.ast;

public class DeclarationStatement extends Statement {
	public StatementKind kind() {return StatementKind.DECLARATION;}
	private Declaration dec;
	public DeclarationStatement(Declaration d) {
		dec = d;
	}
	public Declaration getDeclaration() { return dec; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "" + dec;}
	private boolean isVariableLengthArray(Type t) {
		if (t.kind() == TypeKind.ARRAY && ((Array_Type)t).getExp().kind() != EKind.INTVALUE)
			return true;
		else if (t.kind() == TypeKind.ARRAY)
			return isVariableLengthArray(((Array_Type)t).getType());
		else
			return false;
	}
	private String generateCodeVLA(Array_Type t) {
		String res;//= "set_local $tmp\nget_local $tmp\nget_local $tmp\n";
		if (t.getExp().kind() == EKind.INTVALUE) {
			int value = ((IntegerExpression)t.getExp()).getValue();
			res = "set_local $tmp\nset_local $tmp2\n";
			res = res + "block\nloop\nget_local $tmp2\nget_local $tmp2\ni32.const " + value + "\ni32.lt_s\ni32.eqz\nbr_if 1\nget_local $tmp\ni32.const 0\n" + 
				"get_local $tmp\n";
			res = res + generateCodeVLA((Array_Type)t.getType()) + "i32.const " + t.getType().size() + "\ni32.add\nset_local $tmp\ni32.const 1\ni32.add\nset_local $tmp2\nbr 0\nend\nend\n";
			return res;
		}
		else {
			res = "get_global $SP\ni32.store\nget_global $SP\n" + t.getExp().generateCode();
			if (t.getExp().kind() == EKind.DESIGNATOR)
				res = res + "i32.load\n";

			res = res + "i32.const " + t.getType().size() + "\ni32.mul\ni32.add\nget_global $SP\nset_local $tmp\nset_global $SP\n";
			if (isVariableLengthArray(t.getType())) {
				res = res + "i32.const 0\nset_local $tmp2\n";
				res = res + "block\nloop\nget_local $tmp2\nget_local $tmp2\nget_local $tmp\nset_local $tmp2\n" + t.getExp().generateCode();
				if (t.getExp().kind() == EKind.DESIGNATOR)
					res = res + "i32.load\n";
				res = res + "i32.lt_s\ni32.eqz\nbr_if 1\nget_local $tmp2\ni32.const 0\n" + 
					"get_local $tmp2\n";
				res = res + generateCodeVLA((Array_Type)t.getType()) + "i32.const " + t.getType().size() + "\ni32.add\nset_local $tmp\ni32.const 1\ni32.add\nset_local $tmp2\nbr 0\nend\nend\n";

			}
			res = res + "drop\n";
			return res;
		}
	}
	public String generateCode() {
		Delta.add(dec, dec.getType().size());
		if (isVariableLengthArray(dec.getType())) {
			String res = "i32.const 0\ni32.const " + Delta.getPos(dec) + "\nget_global $current_frame\ni32.add\n";
			return res + generateCodeVLA((Array_Type)dec.getType());
		}
		else
			return "";
	}
	public void maxMemory(MutableInt c, MutableInt max) {
		c.x += dec.getType().size();
		if (c.x > max.x) max.x = c.x;
	}
	public void maxBlocks(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {
		if (isVariableLengthArray(dec.getType())) {
			VLAarray.x = 1;
			anyVLAarray.x = 1;
		}
	}
	public String error_pos() {
		return dec.error_pos();
	}
}
