package main.ast;

public enum ArithmeticKind {
	LOWER,LOWER_EQUAL,GREATER,GREATER_EQUAL,ADD,MINUS,MULT,DIV
}
