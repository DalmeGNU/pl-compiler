package main.ast;

public class ReturnStatement extends Statement {
	public StatementKind kind() {return StatementKind.RETURN;}
	private E exp;
	private int row;
	private int column;
	public ReturnStatement(E e, int arow, int acolumn) {
		exp = e;
		row = arow;
		column = acolumn;
	}
	public E getExp() { return exp; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "Return(" + exp + ")";}
	public String generateCode() {
		if (exp.type().kind() != TypeKind.STRUCT) {
			// again, the pointer-array compatibility
			if (exp.type().kind() == TypeKind.ARRAY) {
				return exp.generateCode() + "return\n";
			}
			else if (exp.kind() == EKind.DESIGNATOR) {
				return exp.generateCode() + "i32.load\nreturn\n";
			} else {
				return exp.generateCode() + "return\n";
			}
		}
		else {
			if (exp.kind() == EKind.DESIGNATOR) {
				int t_size = exp.type().size();
				return exp.generateCode() + "i32.const " + t_size + "\n" +
					"get_global $current_frame\ni32.const " + t_size +
					"\ni32.sub\ncall $mcopy\nreturn\n";
			} else if (exp.kind() == EKind.FUNCTIONCALL) {
				int t_size = exp.type().size();
				return exp.generateCode() + "get_global $SP\ni32.const 8\ni32.add\n" + 
					"i32.const " + t_size + "\nget_global $current_frame\n" + 
					"i32.const " + t_size + "\ni32.sub\ncall $mcopy\nreturn\n";
			} else {
				System.out.println("Compiler fatal error: Expression: " + exp + " returns a type struct, but isn't a designator o a function call");
				System.exit(-1);
				return null;
			}
		}
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
