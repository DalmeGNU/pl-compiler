package main.ast;

import java.util.ArrayList;

public class Struct_Type extends Type {
	private ArrayList<Declaration> l;
	public TypeKind kind() {return TypeKind.STRUCT;}
	public Struct_Type(ArrayList<Declaration> list, int row, int column) {
		l = list;
		this.row = row;
		this.column = column;
	}
	public ArrayList<Declaration> getDeclarations() { return l; }
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return "" + l;}
	public int delta(String field) {
		int value = 0;
		for (Declaration d : l) {
			if (field.equals(d.getName()))
				return value;

			value += d.getType().size();
		}
		// SHOULD BE UNREACHABLE
		// The typing process should have checked
		// that the field exists
		System.out.println("Compiler fatal error: Field " + field + " not found in " + this + " but it was successfully typed");
		System.exit(-1);
		return -1;
	}

	public int size() {
		int s = 0;
		for (Declaration d : l) {
			s += d.getType().size();
		}
		return s;
	}
}
