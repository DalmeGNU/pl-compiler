package main.ast;

public class IfElseStatement extends Statement {
	public StatementKind kind() {return StatementKind.IFELSE;}
	private E cond;
	private Statement if_,else_;
	private int row;
	private int column;
	public IfElseStatement(E cond, Statement if_, Statement else_, int row, int column) {
		this.cond = cond;
		this.if_ = if_;
		this.else_ = else_;
		this.row = row;
		this.column = column;
	}
	public E getCond() { return cond; }
	public Statement getIf() { return if_; }
	public Statement getElse() { return else_; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "IfElse(" + cond + "," + if_ + "," + else_ + ")";}
	public String generateCode() {
		return cond.generateCode() + "if\n" + if_.generateCode() + "else\n" +
			else_.generateCode() + "end\n";
	}
	public void maxMemory(MutableInt c, MutableInt max) {
		MutableInt c1 = new MutableInt();
		MutableInt max1 = new MutableInt();
		if_.maxMemory(c1, max1);
		MutableInt c2 = new MutableInt();
		MutableInt max2 = new MutableInt();
		else_.maxMemory(c2, max2);
		if (c1.x != 0 || c2.x != 0) {
			System.out.println("Compiler fatal error: If-Else " + this + " shouldn't need aditional permanent memory for then statement or else statement");
			System.exit(-1);
		}
		if (c.x+max1.x > max.x)
			max.x = c.x+max1.x;
		if (c.x+max2.x > max.x)
			max.x = c.x+max2.x;
	}
	public void maxMemory(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {
		if_.maxBlocks(c,max,VLAarray,anyVLAarray);
		else_.maxBlocks(c,max,VLAarray,anyVLAarray);
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
