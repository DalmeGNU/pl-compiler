package main.ast;

public class Pointer_Type extends Pointer_Compatible_Type {
	// Pointer to t
	public TypeKind kind() {return TypeKind.POINTER;}
	private Type t;
	public Pointer_Type(Type t, int row, int column) {
		this.t = t;
		this.row = row;
		this.column = column;
	}
	public Type getPointedType() { return t; }
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return "Pointer(" + t + ")";}
	public int size() {
		return 4;
	}
}
