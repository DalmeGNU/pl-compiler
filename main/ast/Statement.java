package main.ast;

public abstract class Statement implements ASTNode {
    public abstract StatementKind kind();
    public NodeKind nodeKind() {return NodeKind.STATEMENT;}
    public String toString() {return "";}
    public void maxMemory(MutableInt c, MutableInt max) {}
    public void maxBlocks(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {}
}
