package main.ast;

// This is "an struct". We only use it to pass information
// upwards on the tree
public class Declaration {
	private String name;
	private  Type type;
	private int row;
	private int column;
	public Declaration(Type atype, String aname, int arow, int acolumn) {
		name = aname;
		type = atype;
		row = arow;
		column = acolumn;
	}

	public String getName() { return name; }

	public Type getType() { return type; }

	public void setType(Type t) { type = t; }

	public String toString() {
		return "" + type + " " + name;
	}

	public String error_pos() {
		return "" + row + ":" + column;
	}

	public int getRow() {
		return row;
	}

	public int getColumn() {
		return column;
	}
}
