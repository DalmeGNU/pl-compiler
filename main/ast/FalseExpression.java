package main.ast;

public class FalseExpression extends E {
	private int row;
	private int column;
	public FalseExpression(int arow, int acolumn) {
		row = arow;
		column = acolumn;
	}
	public EKind kind() {return EKind.FALSE;}
	public String toString() {return "false";}
	public String generateCode() { return "i32.const 0\n"; }
	public String error_pos() {
		return "" + row + ":" + column;
	}
	public int getRow() { return row; }
	public int getColumn() { return column; }
}
