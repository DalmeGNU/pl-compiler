package main.ast;

public class NotEqualExpression extends E {
	public EKind kind() {return EKind.NOT_EQUAL;}
	private E e1;
	private E e2;
	private int row;
	private int column;
	public NotEqualExpression(E a, E b, int arow, int acolumn) {
		e1 = a;
		e2 = b;
		row = arow;
		column = acolumn;
	}
	public String toString() {return "!=(" + e1 + "," + e2 + ")";}
	public E opnd1() { return e1; }
	public E opnd2() { return e2; }
	public String generateCode() {
		String res = ";; " + this.toString() + "\n" + e1.generateCode();
		if (e1.kind() == EKind.DESIGNATOR)
			res = res + "i32.load\n";
		res = res + e2.generateCode();
		if (e2.kind() == EKind.DESIGNATOR)
			res = res + "i32.load\n";

		return res + "i32.ne\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
	public int getRow() { return row; }
	public int getColumn() { return column; }
}
