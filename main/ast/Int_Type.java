package main.ast;

public class Int_Type extends Type {
	public Int_Type(int row, int column) {
		this.row = row;
		this.column = column;
	}
	public TypeKind kind() {return TypeKind.INT;}
	public NodeKind nodeKind() {return NodeKind.TYPE;}
	public String toString() {return "INT";}
	public int size() {
		return 4;
	}
}
