package main.ast;

import java.util.ArrayList;

public class BlockStatement extends Statement {
	public StatementKind kind() {return StatementKind.BLOCK;}
	private ArrayList<Statement> block;
	private int deeplevel = -1;
	// tells us whether there are VLA arrays on this block or not
	// if the VLA array is placed on another block that is inside this one
	// this is false
	private boolean VLAarrays = false;

	private int row;
	private int column;
	public BlockStatement(ArrayList<Statement> b, int row, int column) {
		block = b;
		this.row = row;
		this.column = column;
	}
	public ArrayList<Statement> getBlock() { return block; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "" + block;}
	public String generateCode() {
		String res = "";
		Delta.openBlock();
		if (VLAarrays)
			res = res + "get_global $SP\nset_local $block" + deeplevel + "\n";
		for (Statement s : block)
			res = res + s.generateCode();

		if (VLAarrays)
			res = res + "get_local $block" + deeplevel + "\nset_global $SP\n";

		Delta.closeBlock();

		return res;
	}
	public void maxMemory(MutableInt c, MutableInt max) {
		MutableInt c1 = new MutableInt();
		MutableInt max1 = new MutableInt();
		for (Statement s : block)
			s.maxMemory(c1,max1);

		if (c.x+max1.x > max.x) {
			max.x = c.x+max1.x;
		}
	}
	// VLAarray tells us whether there are VLA arrays on this block or not
	// anyVLAarray tells us whether there are any VLA arrays on this function or not
	public void maxBlocks(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {
		c.x++;
		deeplevel = c.x;
		if (c.x > max.x)
			max.x = c.x;

		MutableInt vla = new MutableInt();
		for (Statement s : block)
			s.maxBlocks(c, max, vla, anyVLAarray);

		if (vla.x == 1)
			VLAarrays = true;

		c.x--;
	}

	public String error_pos() {
		return "" + row + ":" + column;
	}
}
