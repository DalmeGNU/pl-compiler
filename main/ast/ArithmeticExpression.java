package main.ast;

public class ArithmeticExpression extends E {
	public EKind kind() {return EKind.ARITHMETIC;}
	private E e1;
	private E e2;
	private ArithmeticKind op;
	private int row;
	private int column;
	public ArithmeticExpression(E a, ArithmeticKind op, E b, int arow, int acolumn) {
		e1 = a;
		e2 = b;
		this.op = op;
		row = arow;
		column = acolumn;
	}
	public String toString() {return op + "(" + e1 + "," + e2 + ")";}
	public ArithmeticKind akind() { return op; }
	public E opnd1() { return e1; }
	public E opnd2() { return e2; }
	public String generateCode() {
		String res = ";; " + this.toString()  + "\n" + e1.generateCode();
		if (e1.kind() == EKind.DESIGNATOR)
			res = res + "i32.load\n";

		res = res + e2.generateCode();
		if (e2.kind() == EKind.DESIGNATOR)
			res = res + "i32.load\n";

		switch (op) {
			case LOWER:
				return res + "i32.lt_s\n";
			case LOWER_EQUAL:
				return res + "i32.le_s\n";
			case GREATER:
				return res + "i32.gt_s\n";
			case GREATER_EQUAL:
				return res + "i32.ge_s\n";
			case ADD:
				return res + "i32.add\n";
			case MINUS:
				return res + "i32.sub\n";
			case MULT:
				return res + "i32.mul\n";
			case DIV:
				// floores the result
				return res + "i32.div_u\n";
		}
		// should be unreachable
		return null;
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
	public int getRow() { return row; }
	public int getColumn() { return column; }
}
