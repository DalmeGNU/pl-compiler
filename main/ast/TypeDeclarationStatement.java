package main.ast;

public class TypeDeclarationStatement extends Statement {
	public StatementKind kind() {return StatementKind.TYPEDECLARATION;}
	private Declaration dec;
	public TypeDeclarationStatement(Declaration d) {
		dec = d;
	}
	public Declaration getDeclaration() { return dec; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "Typedef(" + dec + ")";}
	public String error_pos() { return dec.error_pos(); }
}
