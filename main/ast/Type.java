package main.ast;

public abstract class Type implements ASTNode {
    protected int row;
    protected int column;
    public abstract TypeKind kind();
    public NodeKind nodeKind() {return NodeKind.TYPE;}
    public String toString() {return "";}
    public abstract int size();
    // true for variable length arrays
    public boolean hasVariableLength() { return false; }
    public int getRow() { return row; }
    public int getColumn() { return column; }
    public String error_pos() { return "" + row + ":" + column; }
}
