package main.ast;

public interface ASTNode {
	default public Type type() {
		return null;
	};
	default public ASTNode bind() {
		return null;
	};
	default public String generateCode() {
		return null;
	}
	public NodeKind nodeKind();
	public String toString();
	default public String error_pos() {
		return "";
	}
}
