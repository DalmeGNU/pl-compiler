package main.ast;

public enum TypeKind {
  // NULLTYPE is used for error recovery on the typing phase
  // It's simply a way to say "No type" / "Type error"
  INT,BOOL,POINTER,ARRAY,STRUCT,CUSTOM,NULLTYPE
}
