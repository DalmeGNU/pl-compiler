package main.ast;

import java.util.HashMap;
import java.util.ArrayList;

public class Delta {
	// hashmap checks for reference equality in declaration
	// (we haven't override equals)
	private static HashMap<Declaration, Integer> delta;
	private static int delta_max;
	private static ArrayList<HashMap<Declaration, Integer>> stack;
	private static ArrayList<Integer> stack_int;
	public static void reset() {
		delta_max = 0;
		delta = new HashMap<>();
		stack = new ArrayList<>();
		stack_int = new ArrayList<>();
	}

	public static void add(Declaration d, int size) {
		delta.put(d, delta_max);
		delta_max += size;
	}

	public static void openBlock() {
		stack.add(delta);
		stack_int.add(delta_max);
	}

	public static void closeBlock() {
		delta = stack.get(stack.size()-1);
		delta_max = stack_int.get(stack_int.size()-1);
		stack.remove(stack.size()-1);
		stack_int.remove(stack_int.size()-1);
	}

	public static int getPos(Declaration d) {
		return delta.get(d);
	}
}
