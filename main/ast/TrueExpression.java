package main.ast;

public class TrueExpression extends E {
	private int row;
	private int column;
	public TrueExpression(int arow, int acolumn) {
		row = arow;
		column = acolumn;
	}
	public EKind kind() {return EKind.TRUE;}
	public String toString() {return "true";}
	public String generateCode() { return "i32.const 1\n"; }
	public String error_pos() {
		return "" + row + ":" + column;
	}
	public int getRow() { return row; }
	public int getColumn() { return column; }
}
