package main.ast;

public class PointerDesignator extends Designator {
	private E d;
	private int row;
	private int column;
	public PointerDesignator(E d, int arow, int acolumn) {
		this.d = d;
		row = arow;
		column = acolumn;
	}
	public E getPointedDesignator() { return d; }
	public DesignatorKind dkind() { return DesignatorKind.POINTER; }
	public String toString() {return "*(" + d + ")";}
	public String generateCode() {
		return d.generateCode() + "i32.load\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
