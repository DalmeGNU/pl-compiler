package main.ast;

public class NullType extends Type {
	public TypeKind kind() {
		return TypeKind.NULLTYPE;
	}
	public int size() { return 0; }
	public String toString() { return "NULLTYPE"; }
}
