package main.ast;

public class FunctionCallStatement extends Statement {
	public StatementKind kind() {return StatementKind.FUNCTIONCALL;}
	private FunctionCall fcall;
	private ASTNode bind_; // bind of the function name
	public FunctionCallStatement(FunctionCall f) {
		fcall = f;
		bind_ = null;
	}
	public ASTNode bind() { return bind_; }
	public void setbind(ASTNode b) { bind_ = b; fcall.setbind(b); }
	public FunctionCall getCall() { return fcall; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "" + fcall;}
	public String generateCode() {
		String res = fcall.generateCode();
		// if functions returns something on the stack, we have to remove it
		// as we're not using it
		if (((FunctionDefinition)bind_).getType().kind() != TypeKind.STRUCT)
			res = res + "drop\n";

		return res;
	}
	public String error_pos() {
		return fcall.error_pos();
	}
}
