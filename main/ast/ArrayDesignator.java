package main.ast;

public class ArrayDesignator extends Designator {
	private E designator;
	// the array position expression (the one in [])
	private E array_exp;
	private int row;
	private int column;
	public ArrayDesignator(E d, E e, int arow, int acolumn) {
		designator = d;
		array_exp = e;
		row = arow;
		column = acolumn;
	}
	public E getDesignator() { return designator; }
	public E getExp() { return array_exp; }
	public DesignatorKind dkind() { return DesignatorKind.ARRAY; }
	public String toString() {return "(" + designator + ")[" + array_exp + "]";}
	public String generateCode() {
		String res = designator.generateCode();
		// if its a dynamic array
		if (designator.type().kind() == TypeKind.ARRAY) {
			Array_Type t = ((Array_Type)designator.type());
			if (t.getExp().kind() != EKind.INTVALUE)
				// in practice a dynamic array is just a "beautiful" pointer
				res = res + "i32.load\n";
		}

		int size = 0;
		if (designator.type().kind() == TypeKind.ARRAY)
			size = ((Array_Type)designator.type()).getType().size();
		else if (designator.type().kind() == TypeKind.POINTER) {
			res = res + "i32.load\n";
			size = ((Pointer_Type)designator.type()).getPointedType().size();
		}
		else {
			System.out.println("Compiler fatal error: Indexed type " + designator.type()  +" isn't array. In " + this);
			System.exit(-1);
		}
		String index_code = array_exp.generateCode();
		if (array_exp.kind() == EKind.DESIGNATOR)
			index_code = index_code + "i32.load\n";

		return res + "i32.const " + size + "\n" + index_code +
			"i32.mul\ni32.add\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
