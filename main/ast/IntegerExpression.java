package main.ast;

// constant integer expressions
public class IntegerExpression extends E {
	public EKind kind() {return EKind.INTVALUE;}
	private String value;
	private int row;
	private int column;
	public IntegerExpression(String v, int arow, int acolumn) {
		value = v;
		row = arow;
		column = acolumn;
	}
	public String toString() {return value;}
	public String generateCode() { return "i32.const " + value + "\n"; }
	public int getValue() { return Integer.parseInt(value); }
	public String error_pos() {
		return "" + row + ":" + column;
	}
	public int getRow() { return row; }
	public int getColumn() { return column; }
}
