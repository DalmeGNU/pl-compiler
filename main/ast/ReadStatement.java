package main.ast;

public class ReadStatement extends Statement {
	public StatementKind kind() {return StatementKind.READ;}
	private E des;
	private int row;
	private int column;
	public ReadStatement(E a, int arow, int acolumn) {
		des = a;
		row = arow;
		column = acolumn;
	}
	public E getDesignator() { return des; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "Read(" + des + ")";}
	public String generateCode() {
		return des.generateCode() + "call $read\ni32.store\n";
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
