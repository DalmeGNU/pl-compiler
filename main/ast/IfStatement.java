package main.ast;

public class IfStatement extends Statement {
	public StatementKind kind() {return StatementKind.IF;}
	private E cond;
	private Statement then;
	private int row;
	private int column;
	public IfStatement(E cond, Statement then, int row, int column) {
		this.cond = cond;
		this.then = then;
		this.row = row;
		this.column = column;
	}
	public E getCond() { return cond; }
	public Statement getIf() { return then; }
	public NodeKind nodeKind() {return NodeKind.STATEMENT;}
	public String toString() {return "If(" + cond + "," + then + ")";}
	public String generateCode() {
		return cond.generateCode() + "if\n" + then.generateCode() + "end\n";
	}

	public void maxMemory(MutableInt c, MutableInt max) {
		then.maxMemory(c,max);
	}
	public void maxBlocks(MutableInt c, MutableInt max, MutableInt VLAarray, MutableInt anyVLAarray) {
		then.maxBlocks(c,max,VLAarray,anyVLAarray);
	}
	public String error_pos() { return "" + row + ":" + column; }
}
