package main.ast;

import java.util.ArrayList;

public class FunctionCall implements ASTNode {
	private ArrayList<E> args;
	private String fname;
	private ASTNode bind_;
	private int row;
	private int column;
	public FunctionCall(String n, int arow, int acolumn, ArrayList<E> a) {
		fname = n;
		args = a;
		row = arow;
		column = acolumn;
	}
	public String getName() { return fname; }
	public ArrayList<E> getArgs() { return args; }
	public NodeKind nodeKind() {return NodeKind.FUNCTIONCALL;}
	public String toString() {return "Call(" + fname + "," + args + ")";}
	public void setbind(ASTNode b) { bind_ = b; }
	public String generateCode() {
		String res = "get_global $SP\nget_global $current_frame\ni32.store\n" +
			"get_global $SP\ni32.const 4\ni32.add\nget_global $SP\ni32.store\n";
		int pos = 8;
		int header_extra_size = 0;
		if (((FunctionDefinition)bind_).getType().kind() == TypeKind.STRUCT) {
			header_extra_size += ((FunctionDefinition)bind_).getType().size();
			pos += header_extra_size;
		}
		res = res + "get_global $SP\nset_local $curSP\nget_global $SP\ni32.const " + (((FunctionDefinition)bind_).maxMemory() + pos) + "\ni32.add\nset_global $SP\n";
		// we test if $NP < $SP
		res = res + "call $checkNPSP\n";
		int i = 0;
		for (E e : args) {
			// the original declaration of this argument on the
			// function definition
			Declaration arg = ((FunctionDefinition)bind_).getArgs().get(i);
			// again we have to distuinguish the case in which we have
			// an array as a pointer compatible type
			if (e.type().kind() == TypeKind.ARRAY && arg.getType().kind() == TypeKind.POINTER) {
				// the only expression that can return an array is a
				// designator
				res = res + "get_local $curSP\ni32.const " + pos + "\ni32.add\n" + e.generateCode();
				if (((Array_Type)e.type()).getExp().kind() != EKind.INTVALUE)
					res = res + "i32.load\n";
				res = res + "i32.store\n";
			}
			// from this point on the type in Declaration arg and in expression
			// e are the same
			else if (e.kind() == EKind.DESIGNATOR) {
				res = res + e.generateCode() + "i32.const " + e.type().size() + "\nget_local $curSP\ni32.const " + pos + "\ni32.add\ncall $mcopy\n";
			}
			else if (e.kind() == EKind.FUNCTIONCALL) {
				if (e.type().kind() != TypeKind.STRUCT)
					res = res + e.generateCode() + "set_local $tmp\n" +
						"get_global $curSP\ni32.const " + pos + "\ni32.add\n" + 
						"get_local $tmp\ni32.store\n";
				else {
					int t_size = e.type().size();
					res = res + e.generateCode() + "get_global $SP\ni32.const 8\ni32.add\n" +
						"i32.const " + t_size + "\nget_local $curSP\ni32.const " + pos + "\ni32.add\ncall $mcopy\n";
				}
			}
			else {
				// in this case result is of type i32
				res = res + "get_local $curSP\ni32.const " + pos + "\ni32.add\n" +
					e.generateCode() + "i32.store\n";
			}
			pos += e.type().size();
			i++;
		}
		res = res + "get_local $curSP\ni32.const " + (header_extra_size + 8) + "\ni32.add\nset_global $current_frame\ncall $" + fname + "\n";
		// restore $current_frame and $SP
		res = res + "get_global $current_frame\ni32.const " + (header_extra_size + 4) + "\ni32.sub\n" +
			"i32.load\nset_global $SP\nget_global $current_frame\n" +
			"i32.const " + (header_extra_size + 8) + "\ni32.sub\ni32.load\nset_global $current_frame\n";
		return res;
	}
	public String error_pos() {
		return "" + row + ":" + column;
	}
}
