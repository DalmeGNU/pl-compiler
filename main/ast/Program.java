package main.ast;

import java.util.*; // import List and ArrayList

public class Program implements ASTNode {
	private ArrayList<FunctionDefinition> functions;
	public Program(ArrayList<FunctionDefinition> functions) {
		this.functions = functions;
	}
	public ArrayList<FunctionDefinition> getFunctions() { return functions; }
	public NodeKind nodeKind() {return NodeKind.PROGRAM;}
	public String toString() {return "" + functions;}
	public String generateCode() {
		String res = "(module\n(import \"runtime\" \"print\" (func $print (param i32)))\n(import \"runtime\" \"read\" (func $read (result i32)))\n" +
			"(import \"runtime\" \"exceptionHandler\" (func $exception (param i32)))\n(memory 2000)\n" +
			"(global $current_frame (mut i32) (i32.const 0))\n(global $SP (mut i32) (i32.const 0))\n(global $NP (mut i32) (i32.const 131071996))\n(start $main)\n";
		res = res + "(func $mcopy\n(param $ini i32)\n(param $size i32)\n(param $dest i32)\n" +
			"(local $i i32)\ni32.const 0\nset_local $i\nblock\nloop\nget_local $i\n" +
			"get_local $size\ni32.lt_s\ni32.eqz\nbr_if 1\nget_local $dest\nget_local $i\ni32.add\nget_local $ini\nget_local $i\n" +
			"i32.add\ni32.load\ni32.store\nget_local $i\ni32.const 4\ni32.add\nset_local $i\nbr 0\nend\nend\n)\n";
		res = res + "(func $checkNPSP\nget_global $SP\nget_global $NP\ni32.gt_u\nif\ni32.const 3\ncall $exception\nend\n)\n";
		res = res + "(func $reserveHeap (param $size i32)\n" +
			"get_local $size\ni32.const 0\ni32.le_s\nif\ni32.const 4\ncall $exception\nend\n" +
			"get_global $NP\n" +
			"get_local $size\ni32.sub\nset_global $NP\ncall $checkNPSP\n)\n";
		for (FunctionDefinition f : functions)
			res = res + f.generateCode();

		return res + "(export \"init\" (func $main))\n)";
	}
}
