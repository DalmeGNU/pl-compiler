package main;

import main.ast.*;
import java.util.*;

public final class ASTTyper {
	public static boolean errors = false;

	private Type function_type;

	private Type expandType(Type t) {
		switch (t.kind()) {
			case POINTER:
				Pointer_Type p = (Pointer_Type) t;
				return new Pointer_Type(expandType(p.getPointedType()), p.getRow(), p.getColumn());
			case ARRAY:
				Array_Type a = (Array_Type) t;
				return new Array_Type(expandType(a.getType()), a.getExp(), a.getRow(), a.getColumn());
			case STRUCT:
				Struct_Type st_type = (Struct_Type) t;
				ArrayList<Declaration> l = st_type.getDeclarations();
				ArrayList<Declaration> l_expanded = new ArrayList<>();
				for (Declaration d : l)
					l_expanded.add(new Declaration(expandType(d.getType()), d.getName(), d.getRow(), d.getColumn()));

				return new Struct_Type(l_expanded, t.getRow(), t.getColumn());
			case CUSTOM:
				TypeDeclarationStatement stm = (TypeDeclarationStatement) t.bind();
				return expandType(stm.getDeclaration().getType());
			case INT:
			case BOOL:
				return t;

		}
		return null;
	}


	private boolean equal_types(Type t1, Type t2) {
		switch (t1.kind()) {
			case POINTER:
				if (t2.kind() == TypeKind.POINTER)
					return equal_types(((Pointer_Type)t1).getPointedType(), ((Pointer_Type)t2).getPointedType());
				else
					return false;
			case ARRAY:
				if (t2.kind() == TypeKind.ARRAY) {
					E exp1 = ((Array_Type)t1).getExp();
					E exp2 = ((Array_Type)t2).getExp();
					if (exp1.kind() != EKind.INTVALUE ||
						exp2.kind() != EKind.INTVALUE) {
						System.out.println("No se permiten asignaciones de arrays dinamicos, ya que no se conoce su tipo");
						return false;
					}
					if (((IntegerExpression)exp1).getValue() != ((IntegerExpression)exp2).getValue()) {
						//System.out.println("Incompatible types: " + t1 + "," + t2);
						return false;
					}
					return equal_types(((Array_Type)t1).getType(),((Array_Type)t2).getType());
				}
				else
					return false;
			case STRUCT:
				if (t2.kind() == TypeKind.STRUCT) {
					Struct_Type st1 = (Struct_Type) t1;
					Struct_Type st2 = (Struct_Type) t2;
					ArrayList<Declaration> l1 = st1.getDeclarations();
					ArrayList<Declaration> l2 = st2.getDeclarations();
					if (l1.size() != l2.size())
						return false;
					for (int i = 0; i < l1.size(); i++)
						if (equal_types(l1.get(i).getType(),l2.get(i).getType()) == false)
							return false;

					return true;
				}
				else
					return false;
			case INT:
			case BOOL:
				return t1.kind() == t2.kind();
		}
		return false;
	}
	// returns if two expanded (that is, without identifier/alias)
	// are equal or not
	// THE ORDEN OF t1 AND t2 MATTERS
	// T1 pointer is compatible with T2 array
	// T2 array isn't compatible with T1 pointer
	// Intuitively, this can be seen as "doing" T1 = T2
	private boolean compatible_types(Type t1, Type t2) {
		switch (t1.kind()) {
			case POINTER:
				if (t2.kind() == TypeKind.POINTER)
					return equal_types(((Pointer_Type)t1).getPointedType(),((Pointer_Type)t2).getPointedType());
				else if (t2.kind() == TypeKind.ARRAY)
					return equal_types(((Pointer_Type)t1).getPointedType(),((Array_Type)t2).getType());
				else
					return false;

			case ARRAY:
				if (t2.kind() == TypeKind.ARRAY) {
					E exp1 = ((Array_Type)t1).getExp();
					E exp2 = ((Array_Type)t2).getExp();
					if (exp1.kind() != EKind.INTVALUE ||
						exp2.kind() != EKind.INTVALUE) {
						System.out.println("No se permiten asignaciones de arrays dinamicos, ya que no se conoce su tipo");
						return false;
					}
					if (((IntegerExpression)exp1).getValue() != ((IntegerExpression)exp2).getValue()) {
						//System.out.println("Incompatible types: " + t1 + "," + t2);
						return false;
					}
					return equal_types(((Array_Type)t1).getType(),((Array_Type)t2).getType());
				}
				else
					return false;
			case STRUCT:
				if (t2.kind() == TypeKind.STRUCT) {
					Struct_Type st1 = (Struct_Type) t1;
					Struct_Type st2 = (Struct_Type) t2;
					ArrayList<Declaration> l1 = st1.getDeclarations();
					ArrayList<Declaration> l2 = st2.getDeclarations();
					if (l1.size() != l2.size())
						return false;
					for (int i = 0; i < l1.size(); i++)
						if (equal_types(l1.get(i).getType(),l2.get(i).getType()) == false)
							return false;

					return true;
				}
				else
					return false;
			case INT:
			case BOOL:
				return t1.kind() == t2.kind();
		}
		return false; // unreachable statement
	}

	private Type type_designator(Designator d) throws Exception {
		Type t3;
		switch (d.dkind()) {
			case ATOMIC:
				ASTNode b = d.bind();
				if (b.nodeKind() == NodeKind.FUNCTIONDEFINITION) {
					FunctionDefinition f = (FunctionDefinition) b;
					for (Declaration dec : f.getArgs()) {
						if (Objects.equals(((AtomicDesignator)d).getName(), dec.getName())) {
							d.settype(dec.getType());
							return dec.getType();
						}
					}
					System.out.println("Compiler fatal error:"+ d.error_pos() + " Corrupted binding in " + d);
					throw new Exception();
				}
				else if (b.nodeKind() == NodeKind.STATEMENT) {
					Statement st = (Statement) b;
					if (st.kind() == StatementKind.DECLARATION) {
						d.settype(((DeclarationStatement)st).getDeclaration().getType());
						return d.type();
					} else if (st.kind() == StatementKind.TYPEDECLARATION) {
						System.out.println("Error:" + d.error_pos() + " " + d + " was defined as a type, but is used as a variable");
						ASTTyper.errors = true;
						return new NullType();
					} else {
						System.out.println("Compiler fatal error:"+ d.error_pos() +" Strange binding to a statement found for " + d);
						throw new Exception();
					}
				}
				else { // we should never reach this point
					System.out.println("Compiler fatal error:" + d.error_pos() +" Strange bind found for designator " + d);
					throw new Exception();
				}
			case POINTER:
				// we have checked on the binding step that the E
				// returned is indeed a designator
				Designator d1 = (Designator)(((PointerDesignator)d).getPointedDesignator());
				Type t2 = type_designator(d1);
				if (t2.kind() != TypeKind.POINTER) {
					if (t2.kind() == TypeKind.NULLTYPE)
						return t2;

					System.out.println("Error:"+ d.error_pos() +" Designator " + d + " dereferences " + d1 + " which is not of pointer type");
					ASTTyper.errors = true;
					return new NullType();
				}
				// else
				t3 = ((Pointer_Type)t2).getPointedType();
				d.settype(t3);
				return t3;
			case ARRAY:
				Designator d2 = (Designator) ((ArrayDesignator)d).getDesignator();
				Type t4 = type_designator(d2);
				if (t4.kind() != TypeKind.ARRAY && t4.kind() != TypeKind.POINTER) {
					if (t4.kind() == TypeKind.NULLTYPE)
						return t4;

					System.out.println("Error:"+ d.error_pos() +" Designator " + d + " indexing " + d2 + " which is not pointer nor array");
					ASTTyper.errors = true;
					return new NullType();
				}
				if (t4.kind() == TypeKind.ARRAY) {
					t3 = ((Array_Type)t4).getType();
					d.settype(t3);
					return t3;
				}
				else {
					t3 = ((Pointer_Type)t4).getPointedType();
					d.settype(t3);
					return t3;
				}
			case STRUCT:
				Designator d3 = (Designator) ((StructDesignator)d).getDesignator();
				String field = ((StructDesignator)d).getField();
				Type t5 = type_designator(d3);
				if (t5.kind() != TypeKind.STRUCT) {
					if (t5.kind() == TypeKind.NULLTYPE)
						return t5;

					System.out.println("Error:"+ d.error_pos() +" Designator " + d + " tries to access field " + field + " of " + d3 + ", but " + d3 + " isn't a struct");
					ASTTyper.errors = true;
					return new NullType();
				}
				// else
				ArrayList<Declaration> lst = ((Struct_Type)t5).getDeclarations();
				for (Declaration dec : lst) {
					if (Objects.equals(dec.getName(), field)) {
						d.settype(dec.getType());
						return dec.getType();
					}
				}
				System.out.println("Error:"+ d.error_pos() +" In " + d + " as no field " + field + " is defined in " + d3);
				ASTTyper.errors = true;
				return new NullType();
		}
		// should be unreachable
		return null;
	}

	private Type type_exp(E exp) throws Exception {
		Type t, t2;
		switch (exp.kind()) {
			case DESIGNATOR:
				return type_designator((Designator)exp);
			case TRUE:
			case FALSE:
				exp.settype(new Bool_Type(exp.getRow(), exp.getColumn()));
				return exp.type();

			case INTVALUE:
				exp.settype(new Int_Type(exp.getRow(), exp.getColumn()));
				return exp.type();
			case OR:
			case AND:
				t = type_exp(exp.opnd1());
				if (t.kind() != TypeKind.BOOL) {
					if (t.kind() == TypeKind.NULLTYPE)
						return t;

					System.out.println("Error:"+ exp.opnd1().error_pos() + " Operand " + exp.opnd1() + " of relational (&&, ||) expression isn't of type bool");
					ASTTyper.errors = true;
					return new NullType();
				}
				t2 = type_exp(exp.opnd2());
				if (t2.kind() != TypeKind.BOOL) {
					if (t2.kind() == TypeKind.NULLTYPE)
						return t2;

					System.out.println("Error:"+ exp.opnd2().error_pos() +" Operand " + exp.opnd2() + " in relational (&&, ||) expression isn't of type bool");
					ASTTyper.errors = true;
					return new NullType();
				}
				exp.settype(t);
				return t;
			case EQUAL:
			case NOT_EQUAL:
				t = type_exp(exp.opnd1());
				t2 = type_exp(exp.opnd2());
				if (t.kind() != TypeKind.INT && t.kind() != TypeKind.BOOL) {
					if (t.kind() == TypeKind.NULLTYPE)
						return t;

					System.out.println("Error:"+ exp.opnd1().error_pos() + " Operand " + exp.opnd1() + " is part an operand in an equality comparison (==, !=), but has no type int or bool");
					ASTTyper.errors = true;
					return new NullType();
				}
				if (t2.kind() != TypeKind.INT && t.kind() != TypeKind.BOOL) {
					if (t2.kind() == TypeKind.NULLTYPE)
						return t2;

					System.out.println("Error:"+ exp.opnd2().error_pos() +" Operand " + exp.opnd2() + " is part an operand in an equality comparison (==, !=), but has no type int or bool");
					ASTTyper.errors = true;
					return new NullType();
				}
				if (t.kind() != t2.kind()) {
					System.out.println("Error:"+ exp.error_pos() +" Operands have different types (one int, one bool) in " + exp);
					ASTTyper.errors = true;
					return new NullType();
				}
				exp.settype(new Bool_Type(exp.getRow(), exp.getColumn()));
				return exp.type();
			case ARITHMETIC:
				t = type_exp(exp.opnd1());
				if (t.kind() != TypeKind.INT) {
					if (t.kind() == TypeKind.NULLTYPE)
						return t;

					System.out.println("Error:"+ exp.opnd1().error_pos() +" Operand " + exp.opnd1() + " is part an operand in an arithmetic expression, but has no type int");
					ASTTyper.errors = true;
					return new NullType();
				}
				t2 = type_exp(exp.opnd2());
				if (t2.kind() != TypeKind.INT) {
					if (t2.kind() == TypeKind.NULLTYPE)
						return t2;

					System.out.println("Error:"+ exp.opnd2().error_pos() +" Operand " + exp.opnd2() + " is part an operand in an arithmetic expression, but has no type int");
					ASTTyper.errors = true;
					return new NullType();
				}
				switch (((ArithmeticExpression)exp).akind()) {
					case ADD:
					case MINUS:
					case MULT:
					case DIV:
						exp.settype(t);
						return t;
					default:
						exp.settype(new Bool_Type(exp.getRow(), exp.getColumn()));
						return exp.type();
				}
			case FUNCTIONCALL:
				if (exp.bind().nodeKind() != NodeKind.FUNCTIONDEFINITION) {
					System.out.println("Error:"+ exp.error_pos() +" In " + exp + ", symbol called isn't a function");
					ASTTyper.errors = true;
					return new NullType();
				}
				FunctionDefinition f_def = (FunctionDefinition) exp.bind();
				exp.settype(type_function_call(f_def, ((FunctionCallExpression)exp).getCall()));
				return exp.type();
			case NEW:
				NewExpression new_exp = (NewExpression) exp;
				t = type_exp(new_exp.getExp());
				if (t.kind() != TypeKind.INT) {
					if (t.kind() == TypeKind.NULLTYPE)
						return t;

					System.out.println("Error:"+ new_exp.error_pos() +" The size in " + new_exp + " must be of type int and " + new_exp.getExp() + " is not");
					ASTTyper.errors = true;
					return new NullType();
				}
				new_exp.settype(new Pointer_Type(expandType(new_exp.getType()), new_exp.getType().getRow(), new_exp.getType().getColumn()));
				return new_exp.type();
		}
		return null;
	}

	private Type type_function_call(FunctionDefinition fd, FunctionCall call) throws Exception {
		ArrayList<E> call_args = call.getArgs();
		ArrayList<Declaration> f_args = fd.getArgs();
		if (f_args.size() != call_args.size()) {
			System.out.println("Error:"+ fd.error_pos() +" In function call to " + fd + ": " + f_args.size() + " arguments expected, but " + call_args.size() + " were given");
			ASTTyper.errors = true;
			return new NullType();
		}
		for (int i = 0; i < call_args.size(); i++) {
			Type type_arg = type_exp(call_args.get(i));
			if (compatible_types(f_args.get(i).getType(), type_arg) == false) {
				if (type_arg.kind() == TypeKind.NULLTYPE)
					continue;

				System.out.println("Error:"+ call.error_pos() +" In argument " + (i+1) + " of function call " + call + ": Argument of type " + f_args.get(i).getType() + " expected, but " + type_arg + " was provided");
				ASTTyper.errors = true;
			}
		}
		return fd.getType();
	}

	private void type_statement(Statement s) throws Exception {
		Type t;
		switch (s.kind()) {
			case DECLARATION:
				DeclarationStatement ds = (DeclarationStatement) s;
				Declaration d = ds.getDeclaration();
				t = expandType(d.getType());
				// we change the type into the expanded one
				d.setType(t);
				break;
			case WHILE:
				WhileStatement w = (WhileStatement) s;
				t = type_exp(w.getCond());
				if (t.kind() != TypeKind.BOOL && t.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ w.getCond().error_pos() +" Expression " + w.getCond() + " isn't of type bool");
					ASTTyper.errors = true;
				}
				type_statement(w.getBody());
				break;
			case PRINT:
				PrintStatement p = (PrintStatement) s;
				t = type_exp(p.getExp());
				if (t.kind() != TypeKind.INT && t.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ p.getExp().error_pos() +" Expression " + p.getExp() + " isn't of type int");
					ASTTyper.errors = true;
				}
				break;
			case RETURN:
				ReturnStatement r = (ReturnStatement) s;
				Type return_type = type_exp(r.getExp());
				if (!compatible_types(function_type, return_type) && return_type.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ r.error_pos() +" Expression in " + r + " is of type " + return_type + " while function should return type " + function_type);
					ASTTyper.errors = true;
				}
				break;
			case BLOCK:
				BlockStatement b = (BlockStatement) s;
				for (Statement st : b.getBlock())
					type_statement(st);

				break;
			case FUNCTIONCALL:
				FunctionCallStatement f = (FunctionCallStatement) s;
				if (f.bind().nodeKind() != NodeKind.FUNCTIONDEFINITION) {
					System.out.println("Error:"+ f.error_pos() +" In function call to " + f + ": Symbol " + f.getCall().getName() + " isn't a function");
					ASTTyper.errors = true;
				}

				type_function_call((FunctionDefinition)f.bind(), f.getCall());
				break;
			case READ:
				ReadStatement rd = (ReadStatement) s;
				Designator des = (Designator) rd.getDesignator();
				if (!compatible_types(type_designator(des),new Int_Type(0,0))) {
					System.out.println("Error:" + des.error_pos() + " Designator " + des + " isn't of type int, in " + rd);
				}
				break;
			case IFELSE:
				IfElseStatement i = (IfElseStatement) s;
				t = type_exp(i.getCond());
				if (t.kind() != TypeKind.BOOL && t.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ i.getCond().error_pos()+" Condition " + i.getCond() + " in " + i + " isn't of type bool");
					ASTTyper.errors = true;
				}
				type_statement(i.getIf());
				type_statement(i.getElse());
				break;
			case IF:
				IfStatement ifstm = (IfStatement) s;
				t = type_exp(ifstm.getCond());
				if (t.kind() != TypeKind.BOOL && t.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ ifstm.getCond().error_pos() +" Condition " + ifstm.getCond() + " in " + ifstm + " isn't of type bool");
					ASTTyper.errors = true;
				}
				type_statement(ifstm.getIf());
				break;
			case ASSIGNMENT:
				AssignmentStatement a = (AssignmentStatement) s;
				t = type_designator((Designator)a.getDesignator());
				Type t2 = type_exp(a.getExp());
				if (!compatible_types(t,t2) && t.kind() != TypeKind.NULLTYPE && t2.kind() != TypeKind.NULLTYPE) {
					System.out.println("Error:"+ a.error_pos() +" In assignment " + a + "left and right have different and non compatible types");
					ASTTyper.errors = true;
				}
				break;
		}
	}

	private void type_function(FunctionDefinition f) throws Exception {
		function_type = f.getType();
		// by now we do not support global variables
		// and thus we won't have anything to expand
		// as no type identifier is defined at this point
		ArrayList<Statement> lst = f.getBody();
		for (Statement s : lst) {
			type_statement(s);
		}
	}

	public void typeAST(Program p) throws Exception {
		ArrayList<FunctionDefinition> functions = p.getFunctions();
		for (FunctionDefinition f : functions) {
			type_function(f);
		}
	}
}
